#ifndef STEREO_PUB_H
#define STEREO_PUB_H

#include <ros/ros.h>
#include <vector>
#include <string>
#include <std_msgs/ByteMultiArray.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "pr2_pretouch_stereo/OpticalDist.h"

using namespace cv;

class StereoPub{
private:
    std::string gripper;
    std::string finger_tip;
    unsigned int rows;
    unsigned int cols;
    std::string raw_sub_topic;
    std::string cam_pub_topic;
    std::string range_pub_topic;
    ros::Publisher cam_pub;
    ros::Publisher range_pub;
    ros::Subscriber raw_sub;
    std::vector<int> time_stamp;
    std::vector<uchar> img_buff;
    const unsigned int max_seq;
    unsigned int seq;
    unsigned int img_errors;
    const unsigned int error_thresh;
    bool debug;

    void rawDataCallback(const std_msgs::ByteMultiArray& msg);
    void flushImage(void);

public:
    StereoPub(std::string& gripper, std::string& finger_tip,
              unsigned int max_seq, unsigned int rows, unsigned int cols,
              unsigned int error_thresh,
              bool debug, ros::NodeHandle& node);
    ~StereoPub();

};

#endif // STEREO_PUB_H
