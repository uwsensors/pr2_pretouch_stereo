#include <ros/ros.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <boost/algorithm/string.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

int main(int argc, char** argv){
    ros::init(argc, argv, "cam_viewer_bayer");
    ros::NodeHandle node;

    std::ifstream file("/home/planc509/stereo_finger_ws/src/pr2_pretouch_stereo/input.txt");
    std::string line("");

    std::vector<std::string> colon_tokens;
    std::vector<std::string> tab_tokens;
    std::vector<uchar> data;
    while(std::getline(file, line)) {
        colon_tokens.clear();
        tab_tokens.clear();
        boost::split(colon_tokens, line, boost::is_any_of(":"));
        if(colon_tokens.size() == 2) {
            boost::split(tab_tokens, colon_tokens[1], boost::is_any_of("\t"));

            for(unsigned int i = 1; i < tab_tokens.size(); i++) {
            //    uchar tmp = (uchar) 16*(strtol(tab_tokens[i].c_str(), NULL, 0)>>4);
               uchar tmp = (uchar) (strtol(tab_tokens[i].c_str(), NULL, 0));
                std::cout << tab_tokens[i] << " " << ((int)tmp) << " ";
                data.push_back(tmp);
            }
            std::cout << std::endl;
        }
    }
    std::cout << data.size() << std::endl;

    int img_width = 176;
    int img_height = 144;
  //  CV_YUV2BGR_YUYV

    Mat img(img_height, img_width, CV_8UC1);
    for(int y = 0; y < img.rows; y++) {
        for(int x = 0; x < img.cols; x++) {
            img.at<uchar>(y, x) = data[y*img_width+x];
        }
    }

    Mat color(img_height, img_width, CV_8UC3);
    cvtColor(img, color, CV_BayerRG2BGR);
/*
    Mat rotated(img_width, img_height, CV_8UC1);
    transpose(img, rotated); */
    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", img );                   // Show our image inside it.

    waitKey(0);

    imshow("Display window", color);
    waitKey(0);
}


