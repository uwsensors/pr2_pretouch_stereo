#include <ros/ros.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <boost/algorithm/string.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

int main(int argc, char** argv){
    ros::init(argc, argv, "cam_viewer");
    ros::NodeHandle node;

    std::ifstream file("/home/planc509/stereo_finger_ws/src/pr2_pretouch_stereo/input.txt");
    std::string line("");

    int img_width = 280;
    int img_height = 210;

    std::vector<std::string> colon_tokens;
    std::vector<std::string> tab_tokens;
//    std::vector<uchar> data;
    char data[2*img_width*img_height];
    int idx = 0;
    while(std::getline(file, line)) {
        colon_tokens.clear();
        tab_tokens.clear();
        boost::split(colon_tokens, line, boost::is_any_of(":"));
        if(colon_tokens.size() == 2) {
            boost::split(tab_tokens, colon_tokens[1], boost::is_any_of("\t"));
            for(unsigned int i = 1; i < tab_tokens.size(); i+=1) {
            //    uchar tmp = (uchar) 16*(strtol(tab_tokens[i].c_str(), NULL, 0)>>4);
               uchar tmp = (uchar) (strtol(tab_tokens[i].c_str(), NULL, 0));
                std::cout << tab_tokens[i] << " " << ((int)tmp) << " ";
                //data.push_back(tmp);
                if(idx < 2*img_width*img_height) {
                    data[idx] = (char) tmp;
                }
                idx++;
            }
            std::cout << std::endl;
        }
    }
    std::cout << idx << std::endl;

  //  CV_YUV2BGR_YUYV
    Mat img(img_height, img_width, CV_8UC2, &data[0]);
    //Mat img(img_height, img_width, CV_8UC1);
 /*   for(int y = 0; y < img.rows; y++) {
        for(int x = 0; x < img.cols; x++) {
            img.at<uchar>(y, x) = data[y*img_width+x];
        }
    }*/
    Mat rgb(img_height, img_width, CV_8UC3);

    cvtColor(img, rgb, CV_YUV2BGR_UYVY);
    Mat rotated(img_width, img_height, CV_8UC3);
    transpose(rgb, rotated);
    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", rotated );                   // Show our image inside it.

    waitKey(0);
}

