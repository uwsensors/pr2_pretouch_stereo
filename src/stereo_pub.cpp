#include "pr2_pretouch_stereo/stereo_pub.h"

StereoPub::StereoPub(std::string& gripper, std::string& finger_tip,
                     unsigned int max_seq, unsigned int rows, unsigned int cols,
                     unsigned int error_thresh, bool debug, ros::NodeHandle& node):
    gripper(gripper), finger_tip(finger_tip), rows(rows), cols(cols),
    cam_pub_topic("finger_cam/"+gripper.substr(0,1)+"_gripper/"+finger_tip.substr(0,1)+"_finger"),
    range_pub_topic("finger_range/"+gripper.substr(0,1)+"_gripper/"+finger_tip.substr(0,1)+"_finger"),
    raw_sub_topic("raw_pressure/"+gripper.substr(0,1)+"_gripper_motor"),
    cam_pub(node.advertise<sensor_msgs::Image>(cam_pub_topic,1000)),
    range_pub(node.advertise<pr2_pretouch_stereo::OpticalDist>(range_pub_topic,1000)),
    raw_sub(node.subscribe(raw_sub_topic, 1000, &StereoPub::rawDataCallback, this)),
    time_stamp(4,-1), img_buff(2*rows*cols, 0), max_seq(max_seq), seq(max_seq), img_errors(0),
    error_thresh(error_thresh), debug(debug){

}

StereoPub::~StereoPub() {

}

void StereoPub::flushImage() {

    Mat img(rows, cols, CV_8UC2, &img_buff[0]);
    Mat rgb(rows, cols, CV_8UC3);

    cvtColor(img, rgb, CV_YUV2BGR_UYVY);
    Mat rotated(cols, rows, CV_8UC3);
    transpose(rgb, rotated);

    if(debug) {
        ROS_INFO("img_errors = %d", img_errors);
        imshow( "Display window", rotated );                   // Show our image inside it.
        waitKey(1);
    }

    cv_bridge::CvImage cv_img;
    cv_img.header.stamp = ros::Time::now();
    cv_img.encoding = sensor_msgs::image_encodings::TYPE_8UC3;
    cv_img.image = rotated;

    if(img_errors <= error_thresh) {
        cam_pub.publish(cv_img.toImageMsg());
    }
}

void StereoPub::rawDataCallback(const std_msgs::ByteMultiArray& msg) {

    // Check if time stamp has changed
    if(msg.data[0] != time_stamp[0] || msg.data[1] != time_stamp[1] ||
       msg.data[2] != time_stamp[2] || msg.data[3] != time_stamp[3]) {

        time_stamp[0] = msg.data[0];
        time_stamp[1] = msg.data[1];
        time_stamp[2] = msg.data[2];
        time_stamp[3] = msg.data[3];
        unsigned int new_seq = msg.data[3] > 0 ? msg.data[3] : msg.data[3] + 256;

        if(seq == max_seq || new_seq > seq) {
            // Flush image first
            while(img_buff.size() < 2*rows*cols) {
                if(img_buff.size() > 2*cols) {
                    for(int i = 0; i < 506; i++) {
                        img_buff.push_back(img_buff[img_buff.size()-2*cols]);
                    }

                } else {
                    for(int i = 0; i < 506; i++) {
                        img_buff.push_back(128);
                    }
                }
                img_errors++;
            }

            flushImage();
            img_buff.clear();
            img_errors = 0;
            seq = max_seq;

        }

        if(seq > new_seq) {
            img_errors += seq-new_seq;
            for(int i = 0; i < seq-new_seq; i++) {
                if(img_buff.size() > 2*cols) {
                    for(int i = 0; i < 506; i++) {
                        img_buff.push_back(img_buff[img_buff.size()-2*cols]);
                    }

                } else {
                    for(int i = 0; i < 506; i++) {
                        img_buff.push_back(128);
                    }
                }
            }
        }

        // Got next data buff
        int first_val = msg.data[6];
        bool pub_range = false;
        for(int i = 6; i < 512 && img_buff.size() < 2*rows*cols; i++) {
            img_buff.push_back((uchar)msg.data[i]);
            if(msg.data[i] != first_val) {
                pub_range = true;
            }
        }
        seq = (new_seq == 1 ? max_seq: new_seq-1);

        if(pub_range) {
            pr2_pretouch_stereo::OpticalDist opt_dist;
            opt_dist.header.seq = seq;
            opt_dist.header.stamp = ros::Time::now();
            opt_dist.frontTipData.resize(1);
            opt_dist.frontTipData[0] = msg.data[4] >= 0 ? msg.data[4] : msg.data[4] + 256;
            opt_dist.centerData.resize(1);
            opt_dist.centerData[0] = msg.data[5] >= 0 ? msg.data[5] : msg.data[5] + 256;
            range_pub.publish(opt_dist);
        }



    }

}

#define DEFAULT_SIDE    "right"
#define DEFAULT_ROWS    210
#define DEFAULT_COLS    280
#define DEFAULT_MAX_SEQ 233
#define DEFAULT_DEBUG false
#define DEFAULT_ERROR_THRESH 10

int main(int argc, char** argv) {

    ros::init(argc, argv, "stereo_pub");
    ros::NodeHandle node;

    std::string gripper(DEFAULT_SIDE);
    std::string finger_tip(DEFAULT_SIDE);
    int rows = DEFAULT_ROWS;
    int cols = DEFAULT_COLS;
    int max_seq = DEFAULT_MAX_SEQ;
    bool debug = DEFAULT_DEBUG;
    int error_thresh = DEFAULT_ERROR_THRESH;

    node.param("/stereo_pub/gripper", gripper, gripper);
    node.param("/stereo_pub/finger_tip", finger_tip, finger_tip);
    node.param("/stereo_pub/rows", rows, rows);
    node.param("/stereo_pub/cols", cols, cols);
    node.param("/stereo_pub/max_seq", max_seq, max_seq);
    node.param("/stereo_pub/debug", debug, debug);
    node.param("/stereo_pub/error_thresh", error_thresh, error_thresh);

    StereoPub stereo(gripper, finger_tip, max_seq,
                       rows, cols, error_thresh, debug, node);
    ROS_INFO("Created pub for %s fingertip of %s gripper", finger_tip.c_str(), gripper.c_str());
    ros::spin();
}
