#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>

using namespace cv;
using namespace std;

std::string int_to_hex( int i )
{
  std::stringstream stream;
  stream << "0x"
         << std::setfill ('0') << std::setw(2)
         << std::hex << i;
  return stream.str();
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "cam_gen_node");
    ros::NodeHandle node;


    Mat img;

    if(argc > 1) {
        img = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    } else {
        img = imread("/home/planc509/stereo_finger_ws/src/pr2_pretouch_stereo/data/test.png");
    }

    if(!img.data )                              // Check for invalid input
    {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    cv::Mat greyMat;
    cv::cvtColor(img, greyMat, CV_BGR2GRAY);

    Mat final(144,176, CV_8UC1);
    ofstream outputFile;
    outputFile.open("/home/planc509/stereo_finger_ws/src/pr2_pretouch_stereo/input.txt");

    for(int i = 0; i < 144; i ++) {
        for(int j = 0; j < 176; j += 4) {
             outputFile << "0x20000134 <frame_buff+8>:";
             for(int k = 0; k < 4; k++) {
                uchar val =  greyMat.at<uchar>((i*greyMat.rows)/144,((j+k)*greyMat.cols)/176);
                outputFile << "\t" << int_to_hex(10) << "\t" << int_to_hex((int)val);
                final.at<uchar>(i, j+k) = val;
             }
             outputFile << std::endl;
        }
    }

    outputFile.close();

    namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
    imshow( "Display window", final );
    waitKey(0);

    std::cout << "Rows: " << img.rows << std::endl;
    std::cout << "Cols: " << img.cols << std::endl;


}
