#include <ros/ros.h>
#include <std_msgs/ByteMultiArray.h>

#define BUFF_SIZE 506
#define START_FRAME 233
#define MAX_VAL 58800
std::vector<int> time_stamp;

void data_callback(const std_msgs::ByteMultiArray& msg) {
    static int cur_frame = START_FRAME;
    static uint16_t cur_val = 0;
    // Check if time stamp has changed
    if(msg.data[0] != time_stamp[0] || msg.data[1] != time_stamp[1] ||
       msg.data[2] != time_stamp[2] || msg.data[3] != time_stamp[3]) {

        time_stamp[0] = msg.data[0];
        time_stamp[1] = msg.data[1];
        time_stamp[2] = msg.data[2];
        time_stamp[3] = msg.data[3];

        int rx_frame = msg.data[4] > 0 ? msg.data[4] : msg.data[4]+256;
        ROS_INFO("%d", rx_frame);
        if(rx_frame > cur_frame) {
            ROS_INFO("Resetting: prev_frame = %d, new_frame = %d", cur_frame, rx_frame);
            cur_frame = rx_frame;
            cur_val = (START_FRAME-cur_frame)*BUFF_SIZE;
        } else if(rx_frame < cur_frame) {
            ROS_WARN("Missed frame %d, skipping to frame %d", cur_frame, rx_frame);
            cur_frame = rx_frame;
            cur_val = cur_val + BUFF_SIZE*(cur_frame-rx_frame);
        }

        for(int i = 0; i < BUFF_SIZE && cur_val < MAX_VAL; i+=2) {
            uint16_t rx_val = (msg.data[(i+1)+5] << 8) | msg.data[i+5];
            if(rx_val != cur_val) {
                ROS_ERROR("Frame %d, Expected val = %d, received val = %d", cur_frame, cur_val, (int)rx_val);
            }
            cur_val++;
        }
        cur_frame--;
    }
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "spi_test");
    ros::NodeHandle node;
    for(int i = 0; i < 4; i++) {
        time_stamp.push_back(0);
    }
    ros::Subscriber sub = node.subscribe("raw_pressure/r_gripper_motor", 1000, data_callback);
    ros::spin();
}
